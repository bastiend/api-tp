var express = require('express');
var router = express.Router();
var _ = require('lodash');
const axios = require('axios');

let films = [{
    titre: "Dents de la mere",
    id: "0"
},
{
    titre: "La Classe Americaine",
    id: "1"
}];

const API_URL = "http://www.omdbapi.com/";
const API_KEY = "1cef1490";

/* GET all films */
router.get('/', (req, res) => {
    res.status(200).json({ films });
    res.send('Films codes en dur');

});

/*GET film by ID*/
router.get('/:id', (req, res) => {
    const { id } = req.params;

    const filmcherche = _.find(films, ["id", id]);

    res.status(200).json({
        message: 'Film trouve !',
        filmcherche
    });
});


/* PUT new user */
router.put('/', (req, res) => {
    //Get the data from request from request
    const { titre } = req.body;

    //create new unique id
    const id = _.uniqueId();
    films.push({ titre, id });
    res.json({
        message: 'Film ajoute ${titre} ${id}',
        filmAjoute: { titre, id }
    });
});


/*UPDATE film*/
router.post('/:id', (req, res) => {
    const { id } = req.params;
    const { titre } = req.body; //Recuperer valeur du titre dans le body

    //find in db
    const filmAUpdater = _.find(films, ["id", id]);
    filmAUpdater.titre = titre; //Donner cette valeur de titre au film a updater

    res.json({
        message: 'update de ${id} avec ${titre}'
    });
});

/*DELETE film*/
router.delete('/:id', (req, res) => {
    const { id } = req.params;

    _.remove(films, ["id", id]);
    res.json({
        message: 'Removed film ${id}'
    });
});



/*GET info by film tilte with omdb*/
router.get('/omdb/:titre', (req, res) => {
    const { titre } = req.params;


    axios.get(`${API_URL}?t=${titre}&apikey=${API_KEY}`)
        .then(function (response) {
            console.log(response.data);
            res.json({
                data: response.data
            });
        })
        .catch(function (error) {
            console.log(error);
            res.json({
                message: 'Erreur'
            });
        });

});


module.exports = router;